<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

 $router->group(['middleware'=>'auth'], function () use ($router) {
    $router->get('/api/videos','VideosController@list');
 });

$router->group([], function () use ($router) {
    $router->post('/api/login','AuthController@login');
    $router->get('/api/me','AuthController@me');
});

fetch('/api/videos',
      {
        headers:{
            'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTY1MzYzNDAzMywiZXhwIjoxNjUzNjM3NjMzLCJuYmYiOjE2NTM2MzQwMzMsImp0aSI6ImtOQlBUTGRDeXdYRng1S1giLCJzdWIiOjIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.VISaIrRt2LIMq1f0-UtEveR7_seMvJyPoUsH4JjGYOw'
    
        }
    }
)

