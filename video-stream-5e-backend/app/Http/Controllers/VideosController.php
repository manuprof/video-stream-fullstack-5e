<?php

namespace App\Http\Controllers;

class VideosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function list(){
        return app('db')->select("SELECT * FROM videos");
    }
}
